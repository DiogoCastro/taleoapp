import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css', '../../assets/materialize.min.css']
})

export class PostsComponent {
  posts:any[];

  constructor(public dataService:DataService) {
    this.dataService.getPost().subscribe(posts => {
      this.posts = posts;
    });
  }

  ngOnInit() { }

  //delete clicked post
  onClickDelete(id) {
    if(window.confirm('Are sure you want to delete this item ?')){
      this.dataService.deletePost(id).subscribe(res => {
        for(let i = 0; i < this.posts.length; i++) {
          if(this.posts[i].id == id) {
            this.posts.splice(i,1);
          }
        }
      })
     }
    
  }
  
}